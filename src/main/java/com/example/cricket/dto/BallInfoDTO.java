package com.example.cricket.dto;

/**
 * have all the fields that are in CSV
 */
public class BallInfoDTO {
    private String battingTeam;
    private String bowlingTeam;
    private Integer matchId;
    private Integer inningId;
    private Integer overSeq;

    public String getBattingTeam() {
        return battingTeam;
    }

    public void setBattingTeam(String battingTeam) {
        this.battingTeam = battingTeam;
    }

    public String getBowlingTeam() {
        return bowlingTeam;
    }

    public void setBowlingTeam(String bowlingTeam) {
        this.bowlingTeam = bowlingTeam;
    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public Integer getInningId() {
        return inningId;
    }

    public void setInningId(Integer inningId) {
        this.inningId = inningId;
    }

    public Integer getOverSeq() {
        return overSeq;
    }

    public void setOverSeq(Integer overSeq) {
        this.overSeq = overSeq;
    }
}
