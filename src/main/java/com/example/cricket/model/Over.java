package com.example.cricket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="over")
public class Over {

    @Id
    @Column(name="over_id")
    private Integer overId;

    @Column(name="match_id")
    private Integer matchId;

    @Column(name="player_id")
    private Integer playerId;

    @Column(name="over_sequence")
    private Integer overSequence;

    public Integer getOverSequence() {
        return overSequence;
    }

    public void setOverSequence(Integer overSequence) {
        this.overSequence = overSequence;
    }

    public Integer getOverId() {
        return overId;
    }

    public void setOverId(Integer overId) {
        this.overId = overId;
    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }
}
