package com.example.cricket.model;

import javax.persistence.*;

@Entity
@Table(name="match")
public class Match {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="match_id")
    private Integer matchId;

    @Column(name="match_location")
    private String matchLoaction;

    @Column(name="date")
    private String date;

    @Column(name="first_team")
    private Integer firstTeam;

    @Column(name="secound_team")
    private Integer secondTeam;

    @Column(name="win")
    private String win;

    @Column(name="match_time")
    private String matchTime;

    @Column(name="team_id")
    private Integer teamId;

    @Column(name="season_id")
    private Integer seasonId;

    public Integer getSecondTeam() {
        return secondTeam;
    }

    public Integer getExternalMatchId() {
        return externalMatchId;
    }

    public void setExternalMatchId(Integer externalMatchId) {
        this.externalMatchId = externalMatchId;
    }

    @Column(name = "external_match_id")
    private Integer externalMatchId;


    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public String getMatchLoaction() {
        return matchLoaction;
    }

    public void setMatchLoaction(String matchLoaction) {
        this.matchLoaction = matchLoaction;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getFirstTeam() {
        return firstTeam;
    }

    public void setFirstTeam(Integer firstTeam) {
        this.firstTeam = firstTeam;
    }

    public void setSecondTeam(Integer secondTeam) {
        this.secondTeam = secondTeam;
    }

    public String getWin() {
        return win;
    }

    public void setWin(String win) {
        this.win = win;
    }

    public String getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(String matchTime) {
        this.matchTime = matchTime;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }
}
