package com.example.cricket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="inning")
public class Inning {

    @Id
    @Column(name="inning_id")
    private Integer inningId;

    @Column(name="match_id")
    private Integer matchId;

    public Integer getInningId() {
        return inningId;
    }

    public void setInningId(Integer inningId) {
        this.inningId = inningId;
    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }
}
