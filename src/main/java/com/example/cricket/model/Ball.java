package com.example.cricket.model;

import net.bytebuddy.dynamic.loading.InjectionClassLoader;

import javax.persistence.*;

@Entity
@Table(name="ball")
public class Ball {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ball_id")
    private Integer ballId;

    @Column(name="ball_type")
    private String ballType;

    @Column(name="match_id")
    private Integer matchId;

    @Column(name="over_id")
    private Integer overId;

    public Ball() {
    }

    @Override
    public String toString() {
        return "Ball{" +
                "ballId=" + ballId +
                ", ballType='" + ballType + '\'' +
                ", matchId=" + matchId +
                ", overId=" + overId +
                '}';
    }

    public Integer getBallId() {
        return ballId;
    }

    public void setBallId(Integer ballId) {
        this.ballId = ballId;
    }

    public String getBallType() {
        return ballType;
    }

    public void setBallType(String ballType) {
        this.ballType = ballType;
    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public Integer getOverId() {
        return overId;
    }

    public void setOverId(Integer overId) {
        this.overId = overId;
    }
}
