package com.example.cricket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="team_player_mapping")
public class TeamPlayerMapping {

    @Id
    @Column(name="team_player_id")
    private Integer teamPlayerId;

    @Column(name="player_id")
    private Integer playerId;

    @Column(name="team_id")
    private Integer teamId;

    @Column(name="season_id")
    private Integer seasonId;

    @Column(name="run")
    private Integer run;

    @Column(name="6s")
    private Integer six;

    @Column(name="4s")
    private Integer four;

    @Column(name="100")
    private Integer hundrade;

    @Column(name="total_run")
    private Integer totalRun;

    @Column(name="current_run")
    private Integer currentRun;

    @Column(name="bold_by")
    private Integer boldBy;

    @Column(name="bold_type")
    private String boldType;

    @Column(name="total_over")
    private Integer totalOver;

    @Column(name="white_ball")
    private Integer whiteBall;

    @Column(name="no_ball")
    private Integer noBall;

    public Integer getTeamPlayerId() {
        return teamPlayerId;
    }

    public void setTeamPlayerId(Integer teamPlayerId) {
        this.teamPlayerId = teamPlayerId;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public Integer getRun() {
        return run;
    }

    public void setRun(Integer run) {
        this.run = run;
    }

    public Integer getSix() {
        return six;
    }

    public void setSix(Integer six) {
        this.six = six;
    }

    public Integer getFour() {
        return four;
    }

    public void setFour(Integer four) {
        this.four = four;
    }

    public Integer getHundrade() {
        return hundrade;
    }

    public void setHundrade(Integer hundrade) {
        this.hundrade = hundrade;
    }

    public Integer getTotalRun() {
        return totalRun;
    }

    public void setTotalRun(Integer totalRun) {
        this.totalRun = totalRun;
    }

    public Integer getCurrentRun() {
        return currentRun;
    }

    public void setCurrentRun(Integer currentRun) {
        this.currentRun = currentRun;
    }

    public Integer getBoldBy() {
        return boldBy;
    }

    public void setBoldBy(Integer boldBy) {
        this.boldBy = boldBy;
    }

    public String getBoldType() {
        return boldType;
    }

    public void setBoldType(String boldType) {
        this.boldType = boldType;
    }

    public Integer getTotalOver() {
        return totalOver;
    }

    public void setTotalOver(Integer totalOver) {
        this.totalOver = totalOver;
    }

    public Integer getWhiteBall() {
        return whiteBall;
    }

    public void setWhiteBall(Integer whiteBall) {
        this.whiteBall = whiteBall;
    }

    public Integer getNoBall() {
        return noBall;
    }

    public void setNoBall(Integer noBall) {
        this.noBall = noBall;
    }
}
