package com.example.cricket.response;

public class ResponsePojo {

    private String result;
    private Integer id;

    public ResponsePojo(String result, Integer id) {
        this.result = result;
        this.id = id;
    }

    public void setEmp_id(Integer id) {
        this.id = id;
    }

    public ResponsePojo(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
