package com.example.cricket.repository;

import com.example.cricket.model.Over;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OverRepository extends JpaRepository<Over,Integer> {
}
