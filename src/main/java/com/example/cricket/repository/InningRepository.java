package com.example.cricket.repository;

import com.example.cricket.model.Inning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InningRepository extends JpaRepository<Inning,Integer> {
}
