package com.example.cricket.service;

import com.example.cricket.model.Ball;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.cricket.repository.BallRepository;


@Service
public class BallService {
    @Autowired
    BallRepository ballRepository;

    public void deleteBall(Integer id) {
        ballRepository.deleteById(id);
    }

    public Ball createBall(Ball ball) {
        Ball savedBall = ballRepository.save(ball);
        return savedBall;
    }

    public Ball updateBall(Ball ball) {
        if(ball.getBallId()!= null){

        }
        return null;
    }
}

