package com.example.cricket.service;

import com.example.cricket.model.Owner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.cricket.repository.OwnerRepository;
import com.example.cricket.response.ResponsePojo;

@Service
public class OwnerService {

    @Autowired
    OwnerRepository ownerRepository;

    public void deleteOwner(Integer id)
    {
        ownerRepository.deleteById(id);
    }

    public Owner createOwner(Owner owner)
    {
        Owner savedOwner = ownerRepository.save(owner);
        return savedOwner;
    }
}
