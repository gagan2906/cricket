package com.example.cricket.service;

import com.example.cricket.model.Ball;
import com.example.cricket.model.Inning;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.cricket.repository.InningRepository;
import com.example.cricket.response.ResponsePojo;

@Service
public class InningService {

    @Autowired
    InningRepository inningRepository;

    public void deleteInning(Integer id)
    {
        inningRepository.deleteById(id);
    }

    public Inning createInning(Inning inning)
    {
        Inning savedInning = inningRepository.save(inning);
        return savedInning;
    }
}
