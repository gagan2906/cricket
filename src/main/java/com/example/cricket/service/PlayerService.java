package com.example.cricket.service;

import com.example.cricket.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.cricket.repository.PlayerRepository;
import com.example.cricket.response.ResponsePojo;

@Service
public class PlayerService {

    @Autowired
    PlayerRepository playerRepository;

    public void deletePlayer(Integer id)
    {
        playerRepository.deleteById(id);
    }

    public Player createPlayer(Player player)
    {
        Player savedPlayer = playerRepository.save(player);
        return savedPlayer;
    }
}
