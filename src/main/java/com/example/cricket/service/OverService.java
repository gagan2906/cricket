package com.example.cricket.service;

import com.example.cricket.model.Match;
import com.example.cricket.model.Over;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.cricket.repository.OverRepository;
import com.example.cricket.response.ResponsePojo;

@Service
public class OverService {

    @Autowired
    OverRepository overRepository;

    public void deleteOver(Integer id)
    {
        overRepository.deleteById(id);
    }

    public Over createOver(Over over)
    {
        Over savedOver = overRepository.save(over);
        return savedOver;
    }
}
