package com.example.cricket.service;

import com.example.cricket.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.cricket.repository.TeamRepository;
import com.example.cricket.response.ResponsePojo;

@Service
public class TeamService {

    @Autowired
    TeamRepository teamRepository;

    public void deleteTeam(Integer id)
    {
        teamRepository.deleteById(id);
    }

    public Team createTeam(Team team)
    {
        Team savedTeam = teamRepository.save(team);
        return savedTeam;
    }
}
