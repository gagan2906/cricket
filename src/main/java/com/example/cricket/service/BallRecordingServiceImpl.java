package com.example.cricket.service;

import com.example.cricket.dto.BallInfoDTO;
import com.example.cricket.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class BallRecordingServiceImpl implements BallRecordingService{
    @Autowired
    TeamService teamService;

    //transformer method
    private Team getFirstTeam(BallInfoDTO dto) {
        Team firstTeam = new Team();
        firstTeam.setTeamName(dto.getBattingTeam());
        return firstTeam;
    }

    //transformer method
    private Team getSecondTeam(BallInfoDTO dto) {
        Team secondTeam = new Team();
        secondTeam.setTeamName(dto.getBowlingTeam());

        return secondTeam;
    }

    //for Inning
    private Inning getInning(BallInfoDTO dto, Match match) {

        //3. inning table
        Inning inning = new Inning();
        inning.setInningId(dto.getInningId());
        inning.setMatchId(match.getMatchId());

        return inning;

    }

    //for over method
    private Over getOver(BallInfoDTO dto) {

        //4. over table
        Over over = new Over();
        over.setOverSequence(dto.getOverSeq());
        over.setMatchId(getSecondTeam(dto).getTeamId());
        return over;
    }





    @Transactional
    //@Transactional to be explained on 14 May
    @Override
    public void recordBallInformation(BallInfoDTO dto) {
        //1. creating first Team
        Team battingTeam =  getFirstTeam(dto);      //2. insert in match table
        teamService.createTeam(battingTeam);

        //2. creating second Team
        Team bowlingTeam =  getSecondTeam(dto);
        teamService.createTeam(bowlingTeam);

        Match match = new Match();
        //3. insert in match table
        match.setFirstTeam(battingTeam.getTeamId());
        match.setSecondTeam(bowlingTeam.getTeamId());

        //save batsman, non-striker, bowler, fielder







    }
}
