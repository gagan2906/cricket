package com.example.cricket.service;

import com.example.cricket.model.Season;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.cricket.repository.SeasonRepository;
import com.example.cricket.response.ResponsePojo;

@Service
public class SeasonService {

    @Autowired
    SeasonRepository seasonRepository;

    public void deleteSeason(Integer id)
    {
        seasonRepository.deleteById(id);
    }

    public Season createSeason(Season season)
    {
        Season savedSeason = seasonRepository.save(season);
        return savedSeason;
    }
}
