package com.example.cricket.service;

import com.example.cricket.dto.BallInfoDTO;

public interface BallRecordingService {
    void recordBallInformation(BallInfoDTO dto);
}
