package com.example.cricket.service;

import com.example.cricket.model.Match;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.cricket.repository.MatchRepository;
import com.example.cricket.response.ResponsePojo;

@Service
public class MatchService {

    @Autowired
    MatchRepository matchRepository;

    public void deleteMatch(Integer id)
    {
        matchRepository.deleteById(id);
    }

    public Match createMatch(Match  match)
    {
        Match savedMatch = matchRepository.save(match);
        return savedMatch;
    }
}
