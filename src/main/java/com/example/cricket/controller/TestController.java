package com.example.cricket.controller;

import com.example.cricket.model.Ball;
import com.example.cricket.service.BallService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/test")
public class TestController {

    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    BallService service;

    @GetMapping("")
    public void test() {
        Ball ball = new Ball();
        ball.setBallType("Wide Ball");
        ball.setMatchId(1);
        ball.setOverId(1);

        service.createBall(ball);
        logger.info("ball created:" + ball);


    }

}
